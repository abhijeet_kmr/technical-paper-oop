# Technical Paper

## Topic/Scenario -

You joined a new project. The project codebase is difficult to manage. Your team lead has asked to study a few concepts and refactor the codebase. Create a report on OOP with code samples.

One of the best possible solutions to the above-mentioned scenario that can help developers to manage the codebase is to implement OOP paradigm.

---

## Introduction of OOP:-

OOP stands for Object Oriented Programming. This is a new feature introduced to JavaScript in ES6 version that is centered around objects rather than functions. It is a coding methodology or pattern or style of programming that helps to restructure the source code of the application or piece of software to improve the operation without altering its core functionalities.

## Why OOP is important and how does it make difference?

If not OOP, we practice procedural programming where we use a set of functions and variables that operate on the data. Since it includes many functions, we tend to copy and paste various lines of codes over and over. Probably making changes to one function breaks other few functions too due to the interdependency of functions on each other.
Here comes Object Oriented Programming into consideration to solve such problems. In OOP we combine a group of related variables and functions into an object. It becomes easier to make changes without affecting existing objects much.

- Benefits of Object Oriented Programing:-

  - This allows us to write well-organized codes.
  - Modular and reusable codes can be achieved.
  - Helps to write easy to debug codes.
  - It is a superior choice to implement especially for medium to large-scale websites or projects.
  - Can be used in almost all the JavaScript frameworks such as ReactJs, VueJs, AngularJs, etc.

In OOP, we create new 'objects' from our code using 'classes'. Classes here can be understood as a blueprint that can be used to create new objects based on the rules described in the class.
For instance, an architect develops a blueprint to plan and describe a house. Using the blueprint many real houses can be built in the real world. Similarly, using classes (blueprint or an abstract plan) we can build different objects.

- There are 4 fundamental principles of OOP:-

  1. Abstraction
  2. Encapsulation
  3. Inheritance
  4. Polymorphism

  ***

### 1. Abstraction: -

Ignoring or hiding details that don't matter. This allows us to get an overview perspective of the thing we are trying to implement. Abstraction is a way of hiding the implementation details and showing only the functionality to the users.

For instance,

```
fuction Employee(name, age, baseSalary) {
  this.name = name;
  this.age = age;
  this.baseSalary = baseSalary;
  this.monthlyBonus = 5000;

  this.calculateFinalSalary = function() {
    let finalSalary = this.baseSalary + this.monthlyBonus;
    console.log('Final Salary is : ' + finalSalary)
  }

  this.getEmpDetails = function(){
    console.log('Name : '+this.name' | Age : '+ this.age);
  }
}

let emp1 = new Employee(Ram, 34, 50000);
emp1.getEmpDetails();        // returns Name : Ram | Age : 34
emp1.monthlyBonus = 10000;   // manipulated the data
emp1.calculateFinalSalary(); // returns Final Salary is : 60000
```

In the above-mentioned code, information such as calculateFinalSalary and monthlyBonus is exposed and accessible in the global scope to any user. Hence, any user can change monthlyBonus and manipulate the output.
Whereas,

```
fuction Employee(name, age, baseSalary) {
  this.name = name;
  this.age = age;
  this.baseSalary = baseSalary;

  let monthlyBonus = 5000;

  let calculateFinalSalary = function() {
    let finalSalary = baseSalary + monthlyBonus;
    console.log('Final Salary is : ' + finalSalary)
  }

  this.getEmpDetails = function(){
    console.log('Name : '+this.name' | Age : '+ this.age);
    calculateFinalSalary();
  }
}

let emp1 = new Employee(Ram, 34, 50000);
emp1.getEmpDetails(); // returns Name : Ram | Age : 34
                                 Final Salary is : 55000
```

In the above-mentioned code, monthlyBonus and calculateFinalSalary are changed to functional variables to change their scope from global to local. Hence, cannot be accessible from the outside.
This is an example of abstraction, where we hide the implementation details and show only the functionalities to the normal user.

---

### 2. Encapsulation: -

Grouping related variables and functions that operate on each other into objects are called Encapsulation. It is a mechanism of restricting direct access to some of the object's components and building data with methods that operate on that data. It helps to maintain security and controlled access, hiding the implementation and only exposing the behavior. Also, provides the ease of modification of implementation anytime.

```
let baseSalary = 50000;
let overTime = 5;
let rate = 500;

function getWage(baseSalary, overTime, rate){
  return baseSalary + (overTime * rate)
}
```

The above code has three variables and a function that is an example of the implementation of procedural programming where variables and functions are separated or decoupled.
Whereas,

```
let Employee = {
  baseSalary = 50000;
  overTime = 5;
  rate = 500;

  getWage: function(){
    return this.baseSalary + (this.overTime * this.rate)
  }
};
Employee.getWage();
```

Here, we have an 'employee' object with three properties(baseSalary, overTime, rate) and a method 'getWage'. This implementation is better than the last one because the getWage method doesn't have any parameters. All three parameters are modeled as properties of the object 'employee'. All the properties and getWage function are interrelated to each other and part of one unit.

---

### 3. Inheritance: -

It is a process where one class acquires properties (methods and fields) from another class. It helps us to define a new class with all the functionality of a parent class and even allows us to add more properties. It is a great way to produce reusable and modular codes.

```
class Car{

  setName(name){
    this.name = name;
  }

  startEngine(){
    console.log('Engine started of '+this.name);
  }

  stopEngine(){
    console.log('Engine stopped of '+this.name);
  }
}

class Suzuki extends Car{

  topSpeed(speed){
    console.log('Top speed of 'this.name' is '+speed);
  }
}

let myCar = new Suzuki();
myCar.setName('Swift');
myCar.startEngine();      // returns Engine started of Swift
myCar.stopEngine();       // returns Engine stopped of Swift
myCar.topSpeed(190kmph)   // returns Top speed of Swift is 190kmph
```

Here, in the above code sample by looking at the returns, 'Suzuki' as a child class could inherit all the properties of its parent class 'Car' and be able to access properties from its itself too.

---

### 4. Polymorphism: -

The term Polymorphism comes from Greek which means 'many shapes. In the context of OOP, it means that a child class can overwrite a method that it inherited from a patent class. It helps to perform single action (or a single piece of code) in different forms and multiple times.

```
class Shape{
  draw(){
    console.log('I am a generic shape');
  }
}

class Square extends Shape{
  draw(){
    console.log('I am square');
  }
}

class Circle extends Shape{
  draw(){
    console.log('I am circle');
  }
}

let s = new Shape()
s.draw() // returns I am a generic shape

s = new Square()
s.draw() // returns I am square

s = new Circle()
s.draw() // returns I am circle.
```

In the above code, similar methods are created in three different classes of different forms. Even if the draw function is present in parent classes the child class overrides the function and will have preference over the parent class.

---

### Reference Links: -

[GeeksForGeeks](https://www.geeksforgeeks.org/introduction-object-oriented-programming-javascript/)

[Javascript Udemy by Jonas](https://www.udemy.com/course/the-complete-javascript-course/)

[Programming with Mosh](https://www.youtube.com/watch?v=PFmuCDHHpwk&t=1302s)

[YahooBaba-YouTube](https://www.youtube.com/watch?v=ckdDIhQyK_A&t=391s)
